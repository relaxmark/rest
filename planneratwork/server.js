var express = require('express'); // Web Framework
var bodyParser = require("body-parser");
var app = express();
var sql = require('mssql'); // MS Sql Server client

const jwt = require('jsonwebtoken')

const jwtPassPhrase = 'wachtwoord'

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.all('/*', function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, access-control-allow-origin");
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.setHeader('Access-Control-Allow-Credentials', true);

  if ('OPTIONS' === req.method) {
    res.sendStatus(200);
  }
  else {
    next();
  }
});

var sqlConfig = {
	user: 'AzureAdmin',
	password: 'Db2017AA',
	server: 'relax.database.windows.net',
	database: 'PlannerProd',
	options: { encrypt: true }
}

let azureport = process.env.PORT || 8081

// Start server and listen on http://localhost:8081/
var server = app.listen(azureport, function () {
    var host = server.address().address
    var port = server.address().port

		sql.connect(sqlConfig)
		  .then(function() { 
  			console.log('DB ' + sqlConfig.database + ' connected on ' + sqlConfig.server);
			})
			.catch(function(err) {
				console.log(err);
			});

    console.log("app listening at http://%s:%s", host, port)
});

app.get('/test', function (req, res) {
  var request = new sql.Request();

	request.query('select * from dbo.Test')
	  .then(function(recordSet) { 
	  	//console.log(recordSet);
	  	res.end(JSON.stringify(recordSet) ); 
	  })
	  .catch(function(err) { 
	  	console.log(err); 
	  });
})

app.post('/framework/batch', function (req, res) {

	//console.log(req);
	console.log('Framework/Batch: ' + req.body.action + '-' + JSON.stringify(req.body.params));

	let noAuth = [0]
//	let noAuth = [2, 4, 7, 9, 12, 15, 17]

	console.log('noAuth.indexOf(' + req.body.action + ') = ' + noAuth.indexOf(+req.body.action))

	let authenticationNeeded = noAuth.indexOf(+req.body.action) == -1


	if (authenticationNeeded) {
		console.log('token = ' + req.body.token)
		jwt.verify(req.body.token, jwtPassPhrase, (err, payload) => {
			if (err) {
				res.sendStatus(403)
				return
			}

			if (new Date(payload.expiration) < new Date() ) {
				res.sendStatus(401)
				return
			}

			let sqlRequest = new sql.Request();

			sqlRequest.input('action', req.body.action);
			sqlRequest.input('parameters', JSON.stringify(req.body.params));
			
			sqlRequest.execute('CreateBatch', function(err, result, returnValue, affected) {
				if(err) console.log(err);
				
				//console.log(JSON.stringify(recordsets))
				let payload_new = {
					number : payload.number, 
					name : payload.name, 
					email : payload.email
				}

//2019-02-12 : ververs de expiration-date
				jwt.sign(payload_new, jwtPassPhrase, { expiresIn : 60*60 }, function(err, token) {
					if (err) { console.error(err) } 

					let recordsets = result.recordsets
					let recordSet = [];
					recordSet.push( { "token" : token } ) 
					recordsets.splice(0, 0, recordSet)

					res.end(JSON.stringify(result)); // Result in JSON format
				});
			});
		});
	}
	else {
		let sqlRequest = new sql.Request();

		sqlRequest.input('action', req.body.action);
		sqlRequest.input('parameters', JSON.stringify(req.body.params));
		
		sqlRequest.execute('CreateBatch', function(err, result, returnValue, affected) {
			if(err) console.log(err);
			
			console.log(JSON.stringify(result))

			res.end(JSON.stringify(result)); // Result in JSON format
		});
	}	  
})

app.post('/framework/login', ( request, response) => {
	var sqlRequest = new sql.Request();

	sqlRequest.input('action', 1);
	sqlRequest.input('parameters', JSON.stringify(request.body.params));

	sqlRequest.execute('CreateBatch', function(err, result, returnValue, affected) {
	  if(err) console.log(err);
		
		if (result) {
			//console.log(JSON.stringify(result))

			let recordsets = result.recordsets;

			if (recordsets.length) {

				if (recordsets[recordsets.length - 1][0].ProcedureResult) {

					let userRecord = recordsets[1][0];

					//console.log(JSON.stringify(userRecord))

					let payload = { 
						number : userRecord.relationCode, 
						name : userRecord.relationName, 
						email : userRecord.relationEmail
					}

					//console.log(JSON.stringify(payload))

					jwt.sign(payload, jwtPassPhrase, { expiresIn : 60*60 }, function(err, token) {
						if (err) { console.error(err) } 

						let recordSet = [];
						recordSet.push( { "token" : token } ) 
						recordsets.splice(0, 0, recordSet)
						response.end(JSON.stringify(result)); // Result in JSON format
					});
				}
				else {
					response.end(JSON.stringify(result))
				}
			}
		}
	});	
})