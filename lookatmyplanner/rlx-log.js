module.exports = {
    debug : function(type, text) {
        if (isDebug) {
            if (types.indexOf(type.toLowerCase()) != -1) {
                console.log(time() + ' - ' + text)
            }
        }
    },

    write : function(text) {
        console.log(time() + ' - ' + text)
    },

    warn : function(text) {
        console.warn(time() + ' - ' + text)
    },

    error : function(text) {
        console.error(time() + ' - ' + text)
    }
}

let isDebug = true

let types = [
    'session', 
    'login',
    'request',
    'result',
]

function time() {
    let now = new Date();
    
    return ("0" + now.getHours()).slice(-2) + ':' + 
           ("0" + now.getMinutes()).slice(-2) + ':' + 
           ("0" + now.getSeconds()).slice(-2) + '.' + 
           ("00" + now.getMilliseconds()).slice(-3)
}