module.exports = {
    createPayload : function (session, code, name, email) {
        return {
            sessionId : session,
            relationCodeLoggedIn : code,
            relationName : name,
            relationEmail : email,
        }
    },
    refreshJWT : function(payload) {
        return {
            sessionId : payload.sessionId,	
            relationCodeLoggedIn : payload.relationCodeLoggedIn, 
            relationName : payload.relationName, 
            relationEmail : payload.relationEmail,
        }
    }
}