var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'RelaX! REST',
  description: 'RelaX! Software REST Service',
  script: 'C:\\program files\\relax! software\\REST\\RelaX.js'
});

svc.uninstall();