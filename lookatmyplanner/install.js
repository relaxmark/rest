var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'RelaX! REST',
  description: 'RelaX! Software REST Service',
  script: 'C:\\program files\\relax! software\\REST\\RelaX.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();