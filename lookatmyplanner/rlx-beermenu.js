const fs = require('fs');
let batch = require('./rlx-batch')
let FILESYSTEM = require('./rlx-filesystem')

module.exports = {
    getProducts: function(request, response) {
        console.log('IP = ' + request.socket.remoteAddress)

        let admin = request.query.admin

        if (!admin) {
            return response.status(500).send({'error' : 'invalid request'})
        }

        console.log('getProducts - ' + admin)

        let products = {}
        let filePath = FILESYSTEM.PRODUCTS_FILE(admin)

        fs.readFile(filePath, (err, content) => {
            if (err) {
                response.status(500).send({'error' : 'Error loading products (' + filePath + ')'})
                return console.error(err)
            };

          products = JSON.parse(content)
          response.status(200).send(products)
        });
    } ,

    setProducts: function(request, response) {
        if (!request.body.customer || 
            !request.body.beermenu || 
            !request.body.beermenu.Groups || 
            !request.body.beermenu.Groups.length) {
            return response.status(500).send({'error' : 'invalid request'})
        }

        console.log('requeststatus=' + JSON.stringify(batch.checkRequest(request, 999)))

        let filePath = FILESYSTEM.PRODUCTS_FILE(request.body.customer)
        let products = request.body.beermenu
        
        fs.writeFile(filePath, JSON.stringify(products), (err) => {
            if (err) {
                response.status(500).send({'error' : err})
                return console.error(err)
            }

            response.status(200).send({'message': 'products updated'})
        })
    },

    getPlaylist: function(request, response) {
        let admin = request.query.admin

        if (!admin) {
            return response.status(500).send({'error' : 'invalid request'})
        }

        let playlist = {}
        let filePath = FILESYSTEM.PLAYLIST_FILE(admin)

        fs.readFile(filePath, (err, content) => {
            if (err) {
                response.status(500).send({'error' : 'Error loading playlist (' + filePath + ')'})
                return console.error(err)
            };

          playlist = JSON.parse(content)
          response.status(200).send(playlist)
        })
    },

    setPlaylist: function(request, response) {
        if (!request.body.customer || 
            !request.body.playlist) {
            return response.status(500).send({'error' : 'invalid request'})
        }

        console.log('requeststatus=' + JSON.stringify(batch.checkRequest(request, 999)))

        let filePath = FILESYSTEM.PLAYLIST_FILE(admin)
        let playlist = request.body.playlist
        
        fs.writeFile(filePath, JSON.stringify(playlist), (err) => {
            if (err) {
                response.status(500).send({'error' : err})
                return console.error(err)
            }

            response.status(200).send({'message': 'playlist updated'})
        })
    },

    getStyle: function(request, response) {
        let name = request.query.name

        if (!name) {
            return response.status(500).send({'error' : 'invalid request'})
        }

        let style = {}
        let filePath = FILESYSTEM.STYLE_FILE(name)

        fs.readFile(filePath, (err, content) => {
            if (err) {
                response.status(500).send({'error' : 'Error loading style (' + filePath + ')'})
                return console.error(err)
            };

            style = JSON.parse(content)
            response.status(200).send(style)
        })        
    },

    getLayout: function(request, response) {
        let name = request.query.name

        if (!name) {
            return response.status(500).send({'error' : 'invalid request'})
        }    
        
        let layout = {}
        
        let filePath = FILESYSTEM.LAYOUT_FILE(name)
        fs.readFile(filePath, (err, content) => {
            if (err) {
                response.status(500).send({'error' : 'Error loading layout (' + filePath + ')'})
                return console.error(err)
            };

            layout = JSON.parse(content)
            response.status(200).send(layout)
        })  
    }
}