let database = require('./rlx-database')
let log = require('./rlx-log')
let mail = require('./rlx-mail')
let tools = require('./rlx-tools')


//TODO : verify van de JWT moet in security.js gemaakt worden...
const jwt = require('jsonwebtoken')
const jwtPassPhrase = 'D1tIsE3nB3stW3lV31l1gW@chtw00rd'

module.exports = {
    checkResult : function(result, request) {
        let answer = {
            success : false,
            error : ''
        }

        if (!result) {
            answer.error = 'SP - result is empty'
            return answer
        }

        if (!result.recordsets) {
            answer.error = 'SP - recordsets-property not found'
            return answer
        }

        if (!result.recordsets.length) {
            answer.error = 'SP - no data in recordsets'
            return answer
        }

       
//AFSPRAAK : de laatste recordset bevat een lijst met namen van datasets!
        let aantalResult = result.recordsets.length;  
        let datasetsDefined = result.recordsets[aantalResult - 1];

//TODO : controle of de veld-definitie overeenkomt met wat we hier verwachten
        let aantalDefinitie = datasetsDefined.length;

        if (aantalResult !== aantalDefinitie) {
            let message = 'Mismatched number of datasets (returned: ' + aantalResult + ', defined: ' + aantalDefinitie + ')';

            mail.sendMail('WARNING - CREATEBATCH : Webservice on ' + database.config.database,
                'MESSAGE:\n' + message +
                '\n\nINPUT:\n' + (request ? JSON.stringify(request.body) : '<unknown>') +
                '\n\nOUTPUT:\n' + JSON.stringify(result.recordsets))
            
//TODO : uiteindelijk mag het aantal datasets niet afwijken van wat je binnenkrijgt, maar voor nu laat ik hem even door...
            // answer.error = message
            // return answer
        }

        let resultIndex = tools.getDatasetIndex(datasetsDefined, "result")

        if (resultIndex < 0) {
            answer.error = 'recordset RESULT not found'
            return answer
        }

        let resultDataset = result.recordsets[resultIndex]

        if (resultDataset.length != 1) {
            answer.error = 'recordset RESULT contains unexpected number of records';
            answer.result = result.recordsets
            return answer
        }

        answer.errorCode = resultDataset[0].ProcedureErrorCode     

        if (!resultDataset[0].ProcedureResult) {
            answer.error = 'ProcedureResult is false'
            answer.result = resultDataset
            return answer
        }

        answer.success = true 
        return answer
    },

    checkRequest : function(request, procedure) {
        let answer = {
            payload : undefined,
            status : 500,
            error : ''
        }

        if (!request.body) {
            let error = 'Invalid request, body undefined or empty'
            mail.sendMail('WARNING - BODY : Webservice on ' + database.config().database, error)
            answer.error= error
            return answer
        }

        if (procedure == undefined) { //check op undefined, aangezien procedure = 0 een geldige waarde is, dus !procedure werkt niet!
            let error = 'Invalid request, procedure not found in body'
            mail.sendMail('WARNING - PROCEDURE: Webservice on ' + database.config().database, error)
            answer.error= error
            return answer
        }

    //TODO : kijken of we procedure 11 moeten splitsen zuiver moeten maken als een procedure waarvoor geen token nodig is,
    //       dus eventuele functies waarbij een token wel vereist is, verplaatsen naar een andere (nieuwe?) procedure
        let noAuth = [0,1]
        let authenticationNeeded = noAuth.indexOf(+procedure) == -1  //als de procedure niet voorkomt in de array, dan is een token vereist

        if (authenticationNeeded) {
            let token = tools.getPropertyValue(request.body, "token")

            if (!token) {
                token = request.headers.authorization
                token = token.substring(7)
            }

            if (!token) {
                let error = 'Invalid request, token needed for procedure ' + procedure
                mail.sendMail('WARNING - TOKEN : Webservice on ' + database.config().database, error)
                answer.error= error
                answer.status = 403
                return answer
            }

            log.debug('TOKEN = ' + token)
    
            let verifiedPayload = undefined

            jwt.verify(token, jwtPassPhrase, (err, payload) => {
                if (err) {
                    log.write(JSON.stringify(err))
                    answer.error= 'invalid token'
                    answer.status = 403
                    return answer
                }
    
//TODO : komt ie hier wel bij expiration? of geeft de verify dan ook een error en krijgen we altijd een 403
                if (new Date(payload.expiration) < new Date() ) {
                    answer.error= 'token expired'
                    answer.status = 401
                    return answer
                }

                verifiedPayload = payload
            })

        //JWT.verify verloopt a-synchroon
        //direct return verifiedPayload zal ervoor zorgen dat je altijd undefined terugkrijgt
        //de check functie wacht tot verifiedPayload een waarde gekregen heeft, doordat
        //de setTimeout de check-functie opnieuw aanroept na 200ms
            let check = function() {
                if (verifiedPayload) {
                    return
                }
                else {
                    setTimeout(check, 200)
                }
            }

            check()

            answer.status = 200
            answer.payload = verifiedPayload
            return answer
        }
        else {
            answer.status = 200
            return answer
        }
    },

    processResult : function(result, version, token) {
        // log.debug('processResult - start')

        if (version === 999) {
            return result.recordsets
        }

        else if (version === 1) {
            // log.debug('processResult - afhandeling versie 1')

            let aantalResult = result.recordsets.length;  
            let datasetsDefined = result.recordsets[aantalResult - 1];

            // log.debug('processResult - ' + JSON.stringify(datasetsDefined))


            // Plak de dataset-namen en de datasets achter elkaar 
            let atl = result.recordsets.length;
            let output = '{ "token":' + (token ? '"' + token + '"' : 'null');
            
            for (let r = 0; r < atl - 1; r++) {
                // log.debug('processResult - r = ' + r)

                if (r === atl - 2) {
                    output = output +
                        ',"' + datasetsDefined[r].DatasetName + '": ' +
                        JSON.stringify(result.recordsets[r][0]);
                }
                else {
                    output = output +
                        ',"' + datasetsDefined[r].DatasetName + '": ' +
                        JSON.stringify(result.recordsets[r]);
                }
            }
            output = output + ' }';

            return JSON.parse(output);
        }

        else {
            if (token) {
                let recordsets = result.recordsets
                let recordSet = [];
                recordSet.push( { "token" : token } ) 
                recordsets.splice(0, 0, recordSet)
            }

            return result  
        }
    },
}