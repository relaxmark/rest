PATH = 'w:/inetpub/wwwroot/beermenu'

module.exports = {
    DATA_PATH(administration) {
        return PATH + '/data/' + administration
    },

    STYLE_PATH() {
        return PATH + '/assets/styles'
    },

    LAYOUT_PATH() {
        return PATH + '/data/' + administration + '/layout'
    },

    PRODUCTS_FILE(administration) {
        return this.DATA_PATH(administration) + '/' + administration + '_Products.json'
    },

    PLUS_FILE(administration) {
        return this.DATA_PATH(administration) + '/' + administration + '_PLUs.json'
    },

    PLAYLIST_FILE(administration) {
        return this.DATA_PATH(administration) + '/' + administration + '_Playlist.json'
    },

    STYLE_FILE(style) {
        return this.STYLE_PATH() + '/' + style + '.json'    
    },

    LAYOUT_STYLE(administration, layout) {
        return this.LAYOUT_PATH() + '/' + administration + '_' + layout + '.json'    
    }
}