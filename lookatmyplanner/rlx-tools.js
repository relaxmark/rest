let log = require('./rlx-log')

module.exports = {
    getDatasetIndex : function (datasets, name) {
        let index = -1
    
        for (let i = 0; i < datasets.length; i++) {
            if (datasets[i].DatasetName) {
                if (name.toLowerCase() == datasets[i].DatasetName.toLowerCase()) {
                    index = i
                }
            }
        }
        
        return index
    },    
    getContextValue : function (dataset, name) {
        let result = ''
    
        for (let i = 0; i < dataset.length; i++) {
            if (dataset[i].ResultName.toLowerCase() == name.toLowerCase()) {
                result = dataset[i].ResultValue
            }
        }
    
        return result
    },

    getPropertyValue : function (object, property) {
        let result = undefined

        if (!object) {
            log.warn('GETPROPERTYVALUE - OBJECT UNDEFINED')
            return result
        }

        if (!property) {
            log.warn('GETPROPERTYVALUE - NO PROPERTY SPECIFIED')
            return result
        } 

        let count = 0

        for (let key in object) {
            if (object.hasOwnProperty(key)) {
                if (key.toLowerCase() == property.toLowerCase()) {
                    result = object[key]
                    count++
                }
            }
        }

        if (count == 0) {
            log.warn('GETPROPERTYVALUE - PROPERTY NOT FOUND - "' + property + '"')
        }

        if (count > 1) {
            log.warn('GETPROPERTYVALUE - MULTIPLE PROPERTIES FOUND - "' + property + '"')
        }

        return result
    }
}