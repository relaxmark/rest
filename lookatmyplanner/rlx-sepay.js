const crypto = require('crypto')
const fs = require('fs')
const axios = require('axios')
const xmlparser = require('xml-js')

const baseURL = 'https://services.sepay.nl/SepayWECR/WECR.asmx?op='

const privateKey = fs.readFileSync('Sepay_WECR.pem').toString('ascii');
const publicKey = fs.readFileSync('Sepay_WECR.crt').toString('ascii');

function sign(data) {
    const sha256 = crypto.createSign('RSA-SHA256')
    const hash = sha256.update(data)
    return hash.sign(privateKey, 'base64')
}

function header(input, method) {
    return {
        'Host': 'services.sepay.nl',
        'SOAPAction' : 'https://services.sepay.nl/WECR/' + method,
        'Content-Type': 'text/xml; charset=utf-8',
        'Content-Length': input.length
    }
}

function generateTransaction() {
    let number = parseInt(fs.readFileSync('Sepay_Transaction.rlx').toString('ascii')) + 1;

    fs.writeFileSync('Sepay_Transaction.rlx', number.toString())

    return number;
}

function validateResult(response, method) {
    //console.log('validateResult - ' + method + ' - ' + response.data)

    if (!response.data) { return { success : false } }
    
    const output = xmlparser.xml2json(response.data, {compact: true, spaces: 2})
    const outputJS = JSON.parse(output)

    input = outputJS['soap:Envelope']['soap:Body'][method + 'Response'][method + 'Result']

    //console.log('input = ' + JSON.stringify(input));

    // input = JSON.parse(JSON.stringify(input))

    // console.log('input2 = ' + JSON.stringify(input));

    const properties = Object.getOwnPropertyNames(input)

    let data = ''

    for (i = 0; i < properties.length; i++) {
        if (properties[i] == 'signature') { continue }

        //console.log(properties[i] + ' - ' + input[properties[i]]._text)

        if (i != 0) { data = data + ';' }

        let value = ''

        if (input[properties[i]]._text) { value = input[properties[i]]._text }
        if (properties[i] == 'amount') { value = parseFloat(value).toFixed(2) }

        data = data + value
    }

    //console.log('validateResult - data = ' + data)

    const signature = input.signature._text

    //console.log('validateResult - signature = ' + signature)
    //console.log('validateResult - publicKey = ' + publicKey)

    const sha256 = crypto.createVerify('RSA-SHA256');
    const verifier = sha256.update(data)
    
    let verified = verifier.verify(publicKey, signature, 'base64')

    //console.log('validateResult - verified = ' + verified)

    return { success : verified, result : input }
}

module.exports = {
    startTransaction: function(request, response) {
        const method = 'StartTransaction'

        const sid = request.body.sid
        const login = request.body.login
        const transaction = generateTransaction()
        const amount = request.body.amount

        const input = '0;1.0;' + login + ';' + sid + ';' + transaction + ';C' + transaction + ';' + amount

//        console.log('input = ' + input)

        const signature = sign(input) 

        const xmlInput = 
            '<?xml version="1.0" encoding="utf-8"?>'+
            '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                '<soap:Body>' +
                    '<' +  method + ' xmlns="https://services.sepay.nl/WECR">' +
                        '<key_index>0</key_index>' +
                        '<version>1.0</version>' +
                        '<login>' + login + '</login>' +
                        '<sid>' + sid + '</sid>' +
                        '<transactionref>' + transaction + '</transactionref>' +
                        '<merchantref>C' + transaction + '</merchantref>' +
                        '<amount>' + amount + '</amount>' +
                        '<signature>' + signature + '</signature>' +
                    '</' + method + '>' +
                '</soap:Body>' +
            '</soap:Envelope>'
        
        const headers = header(xmlInput, method)

        axios.post(baseURL + method, xmlInput, { headers: headers }).
            then((res) => { 
                if (validateResult(res, method)) {
                    response.status(200).send('{ "transaction" : "' + transaction + '"}')
                }
                else {
                    response.status(500).send({ error : 'invalid signature' })
                }
            }).
            catch((error) => { 
                response.status(500).send(error) 
            })
    },

    cancelTransaction: function(request, response) {
        const method = 'CancelTransaction'

        const sid = request.body.sid
        const login = request.body.login
        const transaction = request.body.transaction

        const input = '0;1.0;' + login + ';' + sid + ';' + transaction
        const signature = sign(input) 

        const xmlInput = 
            '<?xml version="1.0" encoding="utf-8"?>' +
            '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                '<soap:Body>' +
                    '<' +  method + ' xmlns="https://services.sepay.nl/WECR">' +
                        '<key_index>0</key_index>' +
                        '<version>1.0</version>' +
                        '<login>' + login + '</login>' +
                        '<sid>' + sid + '</sid>' +
                        '<transactionref>' + transaction + '</transactionref>' +
                        '<signature>' + signature + '</signature>'+
                    '</' + method + '>' +
                '</soap:Body>'+
            '</soap:Envelope>'

        const headers = header(xmlInput, method)

        axios.post(baseURL + method, xmlInput, { headers: headers }).
            then((res) => { 
                if (validateResult(res, method)) {
                    response.status(200).send( { cancelled : true })
                }
                else {
                    response.status(500).send({ error : 'invalid signature' })
                }
            }).
            catch((error) => { 
                response.status(500).send(error) 
            })
    },

    statusTransaction: function(request, response) {
        const method = 'GetTransactionStatus'

        const sid = request.body.sid
        const login = request.body.login
        const transaction = request.body.transaction

        const input = '0;1.0;' + login + ';' + sid + ';' + transaction
        const signature = sign(input) 

        const xmlInput = 
            '<?xml version="1.0" encoding="utf-8"?>' +
            '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                '<soap:Body>' +
                    '<' +  method + ' xmlns="https://services.sepay.nl/WECR">' +
                        '<key_index>0</key_index>' +
                        '<version>1.0</version>' +
                        '<login>' + login + '</login>' +
                        '<sid>' + sid + '</sid>' +
                        '<transactionref>' + transaction + '</transactionref>' +
                        '<signature>' + signature + '</signature>'+
                    '</' + method + '>' +
                '</soap:Body>'+
            '</soap:Envelope>'

        const headers = header(xmlInput, method)

        axios.post(baseURL + method, xmlInput, { headers: headers }).
            then((res) => { 
                //console.log('RAW DATA = ' + res.data)

                const _validation = validateResult(res, method)

                if (_validation.success) {
                    console.log(JSON.stringify(_validation.result))

                    response.status(200).send({ status : _validation.result.status._text, message : _validation.result.message._text, input : xmlInput })
                }
                else {
                    response.status(500).send({ error : 'invalid signature' })
                }
            }).
            catch((error) => { 
                response.status(500).send(error) 
            })
    }
}