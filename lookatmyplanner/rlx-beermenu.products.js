const fs = require('fs');
let FILESYSTEM = require('./rlx-filesystem')

module.exports = {
    getPLUs: function(request, response) {
//        console.log('IP = ' + request.socket.remoteAddress)

        let admin = request.query.admin

        if (!admin) {
            return response.status(500).send({'error' : 'invalid request'})
        }

        console.log('getPLUs - ' + admin)

        let products = {}
        let filePath = FILESYSTEM.PLUS_FILE(admin)

        fs.readFile(filePath, (err, content) => {
            if (err) {
                response.status(500).send({'error' : 'Error loading plus (' + filePath + ')'})
                return console.error(err)
            };

          products = JSON.parse(content)
          response.status(200).send(products)
        });

// [
//     { "name": "Hop zij met ons", "price": 7.95, "style": "IPA", "category": "Blond en krachtig", "percentage": 6, "brewery": "Jopen Bier", "country": "NL", "IBU": 65},
//     { "name": "Superb-Owl", "price": 8.95, "style": "Alcoholarm", "category": "Soepel en subtiel", "percentage": 0.2, "brewery": "Uiltje Brewing Company", "country": "NL", "IBU": 30},
//     { "name": "Witte Wolf", "price": 6.75, "style": "Witbier", "category": "Fris en fruitig", "percentage": 6.5, "brewery": "Brouwerij Leeghwater", "country": "NL", "IBU": 50},
//     { "name": "Two Chefs White Mamba", "price": 8.5, "style": "Witbier", "category": "Fris en fruitig", "percentage": 5.1, "brewery": "Two Chefs Brewing", "country": "NL", "IBU": 12},
//     { "name": "Paix-Dieu", "price": 8.95, "style": "Tripel", "category": "Blond en krachtig", "percentage": 10, "brewery": "Brasserie Caulier", "country": "BE", "IBU": 24},
//     { "name": "Le Fort 10", "price": 6.75, "style": "Strong Ale", "category": "Donker en rijk", "percentage": 10, "brewery": "Brouwerij Omer vander Ghinste", "country": "BE", "IBU": 18},
//     { "name": "Crank the Juice", "price": 7.95, "style": "IPA", "category": "Fris en fruitig", "percentage": 6, "brewery": "Brouwerij De Moersleutel", "country": "NL", "IBU": 35},
//     { "name": "Zatte", "price": 8.95, "style": "Tripel", "category": "Blond en krachtig", "percentage": 8, "brewery": "Brouwerij het IJ", "country": "NL", "IBU": 18}
// ]
    }
}