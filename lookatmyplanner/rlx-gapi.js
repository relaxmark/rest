const fs = require('fs');
const {google} = require('googleapis');
let log = require('./rlx-log')

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
const TOKEN_PATH = 'c:/program files/relax! software/gapi/token.json';
const CREDENTIALS_PATH = 'c:/program files/relax! software/gapi/credentials.json';

let credentials = {}

fs.readFile(CREDENTIALS_PATH, (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then start the mainloop.
    credentials = JSON.parse(content);
});



module.exports = {
    getAuthUrl: function(request, response) {
//        log.write('body = ' + JSON.stringify(request.body))
//        log.write('credentials = ' + JSON.stringify(credentials))


        const {client_secret, client_id, redirect_uris} = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(
            client_id, client_secret, redirect_uris[0]);
    
        let url = oAuth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
        });

        response.status(200).send('{ "url" : "' + url + '"}')
    },

    createToken: function(request, response) {
//        log.write('body = ' + JSON.stringify(request.body))
        credentials.code = request.body.code

//        log.write('code = ' + JSON.stringify(code))
//        log.write('credentials = ' + JSON.stringify(credentials))


        const {client_secret, client_id, redirect_uris} = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(
            client_id, client_secret, redirect_uris[0]);

        oAuth2Client.getToken(credentials.code, (err, token) => {
            if (err) {
                response.status(500).send('{"error" : "' + err + '"}')
                return console.error('Error retrieving access token', err);
            }

//            log.write('token = ' + JSON.stringify(token))

            oAuth2Client.setCredentials(token);

// Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) {
                    response.status(500).send('{"error" : "' + err + '"}')
                    return console.error(err)
                }

                response.status(200).send('{"message" : "token successfully generated"}')
            });
        });
    }
}