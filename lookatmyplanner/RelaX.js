let express = require('express'); // Web Framework
let bodyParser = require("body-parser");
let app = express();
let sql = require('mssql'); // MS Sql Server client
let mail = require('./rlx-mail')
let tools = require('./rlx-tools')
let security = require('./rlx-security')
let batch = require('./rlx-batch')
let database = require('./rlx-database')
let log = require('./rlx-log')
let sepay = require('./rlx-sepay')
let GAPI = require('./rlx-gapi')
let beermenu = require('./rlx-beermenu')
let beerPLUs = require('./rlx-beermenu.products')

const jwt = require('jsonwebtoken')
const jwtPassPhrase = 'D1tIsE3nB3stW3lV31l1gW@chtw00rd'

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.all('/*', function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
//  res.setHeader('Access-Control-Allow-Origin', 'http://planner.relaxsoftware.nl);
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, access-control-allow-origin, strict-origin-when-cross-origin, Authorization");
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.setHeader('Access-Control-Allow-Credentials', true);

  if ('OPTIONS' === req.method) {
    res.sendStatus(200);
  }
  else {
    next();
  }
});

let expirationTime = '1h'

let azureport = process.env.PORT || 8081

// Start server and listen on http://localhost:8081/
let server = app.listen(azureport, function () {
	let host = server.address().address
	let port = server.address().port

	sql.connect(database.config())
		.then(function() { 
			log.write('DB connected on ' + database.config().server);
			log.write('---------------------------------------------------------------');
		})
		.catch(function(err) {
			log.write(err);
			mail.sendMail('ERROR : WebService on ' + database.config().database, 'Could not connect to database\n\n' + err);
		});

	log.write("app listening at http://" + host + ":" + port)
});

app.post('/framework/batch', function (request, response) {
	let procedure = tools.getPropertyValue(request.body, 'procedure')
	

//	log.debug('request','\nREQUEST : ' + JSON.stringify(request.body));
	log.debug('request', '\nFRAMEWORK/BATCH : ' + procedure + '-' + JSON.stringify(request.body));



//batch.checkRequest heeft 3 mogelijke antwoorden:
// - de payload van een goedgekeurde token
// - false --> er is iets mis met de inhoud van de request, of met de token
// - true  --> de opdracht heeft geen token nodig en alle verplichte elementen zijn aanwezig 
	let checkRequest = batch.checkRequest(request, procedure) 

	log.debug('session', 'PAYLOAD REQUEST : ' + procedure + '-' + JSON.stringify(checkRequest))

	if (checkRequest.status != 200) {
		log.debug('request','REQUEST : INVALID - ' + checkRequest.error)
		response.status(checkRequest.status).send(checkRequest)
		return
	}


	let sqlRequest = new sql.Request();
	let parameters = tools.getPropertyValue(request.body, "parameters") 

	sqlRequest.input('Procedure', procedure);
	sqlRequest.input('Parameters', JSON.stringify(parameters));

	if (checkRequest.payload) { 
		log.debug('session', 'Session parameter : ' + JSON.stringify(checkRequest.payload))

		sqlRequest.input('Session', JSON.stringify(checkRequest.payload)) 
	}
		
	log.debug('SQL REQUEST EXECUTE - ' + procedure)

	sqlRequest.execute('CreateBatch', function(err, result, returnValue, affected) {
		log.debug('database', 'SQL REQUEST EXECUTED - ' + procedure)

		if(err) {
			log.write(err)
			return
		}

		let action = tools.getPropertyValue(parameters, "action")

		if ((procedure == 9) ||
    		(procedure == 11) ||
		    (procedure == 18 && action == 'EDTWORKORDERDETAIL')) {
			log.debug('result', 'BATCH RESULT ' + procedure + ' = ' + JSON.stringify(result))
		}


//		log.write('RESULT = ' + JSON.stringify(result))

		let checkResult = batch.checkResult(result) 

		if (!checkResult.success) {
			log.write('RESULT FALSE - ' + JSON.stringify(checkResult))

//			log.write('ERROR CODE = ' +checkResult.errorCode)

			if (checkResult.errorCode == 102) {
				checkResult.error = 'Session expired'
				log.write('SESSION EXPIRED' + procedure + ' : ' + checkResult.error)
				response.status(403).send(checkResult)
				return
			}

			if (checkResult.errorCode == 103) {
				checkResult.error = 'Session invalid'
				log.write('SESSION INVALID ' + procedure + ' : ' + checkResult.error)
				response.status(403).send(checkResult)
				return
			}

			if (checkResult.errorCode == 104) {
				checkResult.error = 'Session required'
				log.write('SESSION REQUIRED ' + procedure + ' : ' + checkResult.error)
				response.status(403).send(checkResult)
				return
			}

			log.write('RESPONSE INVALID ' + procedure + ' : ' + checkResult.error)
			response.status(500).send(checkResult)
			return
		}

		//log.debug('REQUEST PAYLOAD ' + procedure + ' = ' + JSON.stringify(checkRequest.payload))

		let version = tools.getPropertyValue(request.body, "version")

		if (checkRequest.payload) {
			log.debug('payload', 'refresh payload...')

			let payload_new = security.refreshJWT(checkRequest.payload)

			log.debug('payload', 'payload refreshed, singing...')
			//log.debug('REQUEST NEW PAYLOAD ' + procedure + ' = ' + JSON.stringify(payload_new))

			jwt.sign(payload_new, jwtPassPhrase, { expiresIn : expirationTime }, function(err, token) {
				log.debug('payload', 'signing finished (error = ' + err + ')')


				if (err) { 
					log.error(err)
					response.status(500).send({ success : false, error : err })
					return 
				} 

				log.debug('payload', 'processing result...')
				result = batch.processResult(result, version, token)
				log.debug('result', 'result = ' + JSON.stringify(result))
				response.status(200).send(JSON.stringify(result)); // Result in JSON format
			});
		}
		else {
			result = batch.processResult(result, version)
			response.status(200).send(JSON.stringify(result)); // Result in JSON format
		}
	})
})

app.post('/framework/login', ( request, response) => {
	let procedure = tools.getPropertyValue(request.body, "procedure")

	log.debug('login', 'LOGIN - START')

	let checkRequest = batch.checkRequest(request, procedure) 

	if (checkRequest.status != 200) {
		log.write('LOGIN - INVALID REQUEST : ' + checkRequest.error + '\n' + JSON.stringify(request.body))

		response.status(checkRequest.status).send('{"success" : false, "error": "' + checkRequest.error + '"}')
		return
	}

	let parameters = tools.getPropertyValue(request.body, "parameters")
	let sqlRequest = new sql.Request();

	sqlRequest.input('procedure', procedure);
	sqlRequest.input('parameters', JSON.stringify(parameters));
	sqlRequest.execute('CreateBatch', function(err, result, returnValue, affected) {
	  if(err) log.write(err);
	
	  log.debug('login', 'LOGIN - ' + JSON.stringify(result))

		let checkResult = batch.checkResult(result, request) 

		if (!checkResult.success) {
			log.write('LOGIN - INVALID RESPONSE : ' + JSON.stringify(checkResult.error))
			response.status(500).send(checkResult)
			return
		}

		let datasets = result.recordsets[result.recordsets.length - 1]
		let action = tools.getPropertyValue(parameters, "action")
		let version = tools.getPropertyValue(request.body, "version")

		if (action == "LOGIN") {
			let contextIndex = tools.getDatasetIndex(datasets, "context"); 

			if (contextIndex < 0) {
				response.status(500).send('{"success" : false, "error": "recordset CONTEXT not found"}')
				return
			}

			let context = result.recordsets[contextIndex] 
			//log.write('CONTEXT = ' + JSON.stringify(context))

			let payload = security.createPayload(
				tools.getContextValue(context, 'sessionID'),
				tools.getContextValue(context, 'relationCode'), 
				tools.getContextValue(context, 'relationName'), 
				tools.getContextValue(context, 'EmailAddress')
			)

			log.debug('session', 'PAYLOAD = ' + JSON.stringify(payload))

			jwt.sign(payload, jwtPassPhrase, { expiresIn : expirationTime }, function(err, token) {
				if (err) { 
					response.status(500).send('{"success" : false, "error": "JWT can not sign payload"}')
					return
				}
				else {
					log.debug('session', 'LOGIN - TOKEN = ' + token)
				} 

				result = batch.processResult(result, version, token)

				log.debug('login', JSON.stringify(result))

				//deze moet binnen de callback van de JWT.sign uitgevoerd worden
				response.status(200).send(JSON.stringify(result));
				return
			})
		}
		else {
			result = batch.processResult(result, version)
			response.status(200).send(JSON.stringify(result));
			return
		}
	});	
})

app.post('/sepay/startTransaction', (request, response) => {
	sepay.startTransaction(request, response)
})

app.post('/sepay/CancelTransaction', (request, response) => {
	sepay.cancelTransaction(request, response)
})

app.post('/sepay/StatusTransaction', (request, response) => {
	sepay.statusTransaction(request, response)
})

app.post('/GAPI/AuthUrl', (request, response) => {
	GAPI.getAuthUrl(request, response)
})

app.post('/GAPI/CreateToken', (request, response) => {
	GAPI.createToken(request, response)
})

app.get('/beermenu/products', (request, response) => {
	beermenu.getProducts(request, response)
})

app.post('/beermenu/products', (request, response) => {
	beermenu.setProducts(request, response)
})

app.get('/beermenu/playlist', (request, response) => {
	beermenu.getPlaylist(request, response)
})

app.post('/beermenu/playlist', (request, response) => {
	beermenu.setPlaylist(request, response)
})

app.get('/beermenu/style', (request, response) => {
	beermenu.getStyle(request, response)
})

app.get('/beermenu/layout', (request, response) => {
	beermenu.getLayout(request, response)
})

app.get('/beermenu/plus', (request, response) => {
	beerPLUs.getPLUs(request, response)
})
